package org.academiadecodigo.ramsters.group_project.sound;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;

public class Sound {

    private Clip clip;
    private Clip ballKicked;
    private URL soundURL;

    public Sound(String pathThemeSong, String pathBallKicked) {
        initClip(pathThemeSong);
        initBallKicked(pathBallKicked);
    }

    public void playTheme(boolean fromStart) {
        if (fromStart) {
            clip.setFramePosition(0);
        }
        FloatControl volume = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
        volume.setValue(0);
        clip.start();
    }

    public void playBallKicked(boolean fromStart) {
        if (fromStart) {
            ballKicked.setFramePosition(0);
        }
        ballKicked.start();
    }

    public void stop() {
        clip.stop();
    }

    public void close() {
        clip.close();
    }

    public int getLength() {
        return clip.getFrameLength();
    }

    public void loopIndef() {
        clip.setLoopPoints(0, (int) (getLength() * 0.94)); //sets loop points at start and end of track
        clip.loop(Clip.LOOP_CONTINUOUSLY); //activates loop
    }

    public void reOpen() {
        AudioInputStream inputStream;
        try {
            inputStream = AudioSystem.getAudioInputStream(soundURL);
            clip.open(inputStream);
        } catch (LineUnavailableException | IOException | UnsupportedAudioFileException e) {
            System.out.println(e.getMessage());
        }
    }

    private void initClip(String path) {
        soundURL = Sound.class.getResource(path); //if loading from jar
        AudioInputStream inputStream;
        try {
            if (soundURL == null) {
                path = path.substring(1);
                File file = new File(path);
                soundURL = file.toURI().toURL(); //if executing on intelliJ
            }
            inputStream = AudioSystem.getAudioInputStream(soundURL);
            clip = AudioSystem.getClip();
            clip.open(inputStream);
        } catch (UnsupportedAudioFileException | LineUnavailableException | IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public void initBallKicked(String path) {
        soundURL = Sound.class.getResource(path); //if loading from jar
        AudioInputStream inputStream;
        try {
            if (soundURL == null) {
                path = path.substring(1);
                File file = new File(path);
                soundURL = file.toURI().toURL(); //if executing on intelliJ
            }
            inputStream = AudioSystem.getAudioInputStream(soundURL);
            ballKicked = AudioSystem.getClip();
            ballKicked.open(inputStream);
        } catch (UnsupportedAudioFileException | LineUnavailableException | IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
