package org.academiadecodigo.ramsters.group_project;

import org.academiadecodigo.ramsters.group_project.game_objects.GameObject;

public class CollisionDetector {

    private GameObject[] objects;

    public CollisionDetector(GameObject[] objects) {
        this.objects = objects;
    }
    public void checkCollisions() {
        // Brute Force Approach (BFA) Iterates between all possible unique pairs. Almost O(N²)
        for (int i = 0; i < objects.length - 1; i++) {
            for (int j = i + 1; j < objects.length; j++) {
                // If distance less or equal to 0 a collision happens.
                if (objects[i].getPosition().distanceTo(objects[j].getPosition()) <= 0) {
                    // Tell the objects they collided and who they collided with (Trough collide Method)
                    objects[i].collide(objects[j]);
                    objects[j].collide(objects[i]);
                }
            }
        }
    }
}
