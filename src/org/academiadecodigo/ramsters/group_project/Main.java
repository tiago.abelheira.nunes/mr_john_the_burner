package org.academiadecodigo.ramsters.group_project;

import org.academiadecodigo.ramsters.group_project.field.Field;

public class Main {
    public static void main(String[] args) throws InterruptedException{

        Game game = new Game(1000,500);
        game.init();
        game.play();


    }
}
