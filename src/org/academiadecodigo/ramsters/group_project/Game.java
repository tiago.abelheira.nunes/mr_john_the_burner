package org.academiadecodigo.ramsters.group_project;

import org.academiadecodigo.ramsters.group_project.field.Field;
import org.academiadecodigo.ramsters.group_project.game_objects.*;
import org.academiadecodigo.ramsters.group_project.sound.Sound;

public class Game {
    private Field field;
    private GameObject[] gameObjects;
    private GameObjectFactory gameObjectFactory;
    private CollisionDetector collisionDetector;
    private Sound sound;

    public Game(int height, int width) {
        field = new Field(height, width);
        gameObjectFactory = new GameObjectFactory(field);
        this.sound = new Sound("/resources/POL-azure-waters-short.wav", "/resources/ball-kicked.wav");
        this.sound = new Sound("/resources/blazer-rail.wav", "/resources/ball-kicked.wav");

    }

    public void init() {
        ScoreBoard scoreBoard = new ScoreBoard();
        gameObjects = gameObjectFactory.createGameObjects(1,1,2);
        GameObjectController controller = new GameObjectController((Player) gameObjects[0], sound);
        ((Player) gameObjects[0]).setBall( (Ball) gameObjects[1]);
        collisionDetector = new CollisionDetector(gameObjects);
        ((Ball)gameObjects[1]).getPosition().setBall((Ball)gameObjects[1]);
        sound.playTheme(true);
        sound.loopIndef();

    }


    public void play() throws InterruptedException{

        while (true) {
            for (GameObject gameObject : gameObjects) {
                    gameObject.move();
                   collisionDetector.checkCollisions();
                    Thread.sleep(5);

            }


        }


    }


}
