package org.academiadecodigo.ramsters.group_project.field;

import org.academiadecodigo.ramsters.group_project.game_objects.Ball;
import org.academiadecodigo.simplegraphicswithoutmargin.graphics.Color;
import org.academiadecodigo.simplegraphicswithoutmargin.graphics.Ellipse;

public class BallPosition extends FieldPosition {

    private Ball ball;
    private Field field;

    public BallPosition(int x, int y, int size, Field field, Color color) {
        super(x, y, size, field, color);
        this.field = field;
    }

    public void setBall(Ball ball) {
        this.ball = ball;
    }

    @Override
    public int[] preventOutOfBounds(int xMove, int yMove) {
        Ellipse shape = super.getShape();

        if (shape.getX() + xMove <= 0) {
            ball.invertX();
            xMove = 0;
        }
        if (shape.getX() + shape.getWidth() + xMove >= field.getWidth()) {
            ball.invertX();
            xMove = 0;
        }


        if (shape.getY() + yMove <= 0) {
            ball.invertY();
            yMove = 0;
        }
        if (shape.getY() + shape.getHeight() + yMove >= field.getHeight()) {
            ball.invertY();
            yMove = 0;
        }
        return new int[]{xMove, yMove};
    }

}

