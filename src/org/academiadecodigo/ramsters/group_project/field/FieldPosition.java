package org.academiadecodigo.ramsters.group_project.field;

import org.academiadecodigo.simplegraphicswithoutmargin.graphics.Color;
import org.academiadecodigo.simplegraphicswithoutmargin.graphics.Ellipse;
import org.academiadecodigo.simplegraphicswithoutmargin.graphics.Rectangle;

import java.util.Random;


public class FieldPosition {

    private Ellipse shape;
    private int centerX;
    private int centerY;
    private int radius;
    private Field field;
    private Color color;

    public FieldPosition(int x, int y, int size, Field field, Color color) {
        shape = new Ellipse(x, y, size, size);
        radius = size / 2;
        centerX = x + radius;
        centerY = y + radius;
        this.field = field;
        this.color = color;
        show();
    }

    public FieldPosition(int size, Field field, Color color) {
        this(new Random().nextInt(field.getWidth()), new Random().nextInt(field.getHeight()), size, field, color);
    }

    public Ellipse getShape() {
        return shape;
    }

    public void setColor(Color color) {
        shape.setColor(color);
        show();
    }

    public int getCenterX() {
        return centerX;
    }

    public int getCenterY() {
        return centerY;
    }

    public void show() {
        shape.fill();
    }

    public void hide() {
        shape.delete();
    }

    public void movePosition(int xMove, int yMove) {
        shape.setColor(color);
        show();
        int[] move = preventOutOfBounds(xMove, yMove);
        shape.translate(move[0], move[1]);
        centerX += move[0];
        centerY += move[1];

    }

    public int[] preventOutOfBounds(int xMove, int yMove) {

        int x = xMove;

        if (shape.getX() + xMove <= 0) {
            x = -shape.getX();
        }
        if (shape.getX() + shape.getWidth() + xMove >= field.getWidth()) {
            x = field.getWidth() - shape.getX() - shape.getWidth();
        }

        int y = yMove;

        if (shape.getY() + yMove <= 0) {
            y = -shape.getY();
        }
        if (shape.getY() + shape.getHeight() + yMove >= field.getHeight()) {
            y = field.getHeight() - shape.getY() - shape.getHeight();
        }

        return new int[]{x, y};
    }

    public int distanceTo(FieldPosition pos) {
        int distance = Math.toIntExact(Math.round(Math.sqrt(Math.pow(centerX - pos.centerX, 2) + Math.pow(centerY - pos.centerY, 2))));
        distance -= (radius + pos.radius);
        return distance;
    }

    public double slopeTo(int centerX, int centerY) {
        // Coordinates of other object as doubles
        double x1 = centerX;
        double y1 = centerY;
        // Coordinates of this object as doubles
        double x2 = this.centerX;
        double y2 = this.centerY;
        // Calculate and return the slope by dividing the rise (change in Y) by the run (change in X)
        if (x1 - x2 == 0) {

            return -Integer.MAX_VALUE;
        }
        return ((y1 - y2) / (x1 - x2));
    }

    public double slopeTo(FieldPosition pos) {
        // Gets coordinates from given position and pass to the standard slopeTo method
        return slopeTo(pos.getCenterX(), pos.getCenterY());
    }
}
