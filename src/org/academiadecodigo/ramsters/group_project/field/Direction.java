package org.academiadecodigo.ramsters.group_project.field;

public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
}
