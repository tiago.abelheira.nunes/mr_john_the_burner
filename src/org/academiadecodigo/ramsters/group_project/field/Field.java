package org.academiadecodigo.ramsters.group_project.field;

import org.academiadecodigo.simplegraphicswithoutmargin.graphics.Color;
import org.academiadecodigo.simplegraphicswithoutmargin.graphics.Rectangle;
import org.academiadecodigo.simplegraphicswithoutmargin.pictures.Picture;

public class Field {

    private Rectangle field;
    private Picture fieldPic;

    public Field(int height, int width) {
        field = new Rectangle(0, 0, height, width);
        field.setColor(Color.GRAY);
        field.fill();

        fieldPic = new Picture(0,0, "resources/map20x10.png");
        fieldPic.draw();
    }

    public int getHeight() {
        return field.getHeight();
    }

    public int getWidth() {
        return field.getWidth();

    }

    public FieldPosition makePosition(int x, int y, int size, Color color) {
        return new FieldPosition(x, y, size, this, color);
    }

    public FieldPosition makePosition(int size, Color color) {
        return new FieldPosition(size, this, color);

    }

    public BallPosition makeBallPosition(int x, int y, int size, Color color) {
        return new BallPosition(x, y, size, this, color);
    }

}
