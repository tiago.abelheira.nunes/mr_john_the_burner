package org.academiadecodigo.ramsters.group_project;

import org.academiadecodigo.ramsters.group_project.field.Direction;
import org.academiadecodigo.ramsters.group_project.game_objects.GameObject;
import org.academiadecodigo.ramsters.group_project.game_objects.Player;
import org.academiadecodigo.ramsters.group_project.sound.Sound;
import org.academiadecodigo.simplegraphicswithoutmargin.keyboard.Keyboard;
import org.academiadecodigo.simplegraphicswithoutmargin.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphicswithoutmargin.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphicswithoutmargin.keyboard.KeyboardHandler;

import java.security.Key;

public class GameObjectController implements KeyboardHandler {

    private Player player;
    private Keyboard keyboard;
    private Sound sound;


    public GameObjectController(Player player, Sound sound) {
        keyboard = new Keyboard(this);
        this.player = player;
        this.sound = sound;
        createKeyboardsEvents();

    }

    private void createKeyboardsEvents() {

        //Pressing

        KeyboardEvent pressingUp = new KeyboardEvent();
        pressingUp.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        pressingUp.setKey(KeyboardEvent.KEY_UP);
        keyboard.addEventListener(pressingUp);

        KeyboardEvent pressingDown = new KeyboardEvent();
        pressingDown.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        pressingDown.setKey(KeyboardEvent.KEY_DOWN);
        keyboard.addEventListener(pressingDown);

        KeyboardEvent pressingLeft = new KeyboardEvent();
        pressingLeft.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        pressingLeft.setKey(KeyboardEvent.KEY_LEFT);
        keyboard.addEventListener(pressingLeft);

        KeyboardEvent pressingRight = new KeyboardEvent();
        pressingRight.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        pressingRight.setKey(KeyboardEvent.KEY_RIGHT);
        keyboard.addEventListener(pressingRight);

        KeyboardEvent pressingSpace = new KeyboardEvent();
        pressingSpace.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        pressingSpace.setKey(KeyboardEvent.KEY_SPACE);
        keyboard.addEventListener(pressingSpace);

        //Releasing

        KeyboardEvent releasingUp = new KeyboardEvent();
        releasingUp.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
        releasingUp.setKey(KeyboardEvent.KEY_UP);
        keyboard.addEventListener(releasingUp);

        KeyboardEvent releasingDown = new KeyboardEvent();
        releasingDown.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
        releasingDown.setKey(KeyboardEvent.KEY_DOWN);
        keyboard.addEventListener(releasingDown);

        KeyboardEvent releasingLeft = new KeyboardEvent();
        releasingLeft.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
        releasingLeft.setKey(KeyboardEvent.KEY_LEFT);
        keyboard.addEventListener(releasingLeft);

        KeyboardEvent releasingRight = new KeyboardEvent();
        releasingRight.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
        releasingRight.setKey(KeyboardEvent.KEY_RIGHT);
        keyboard.addEventListener(releasingRight);

        KeyboardEvent releasingSpace = new KeyboardEvent();
        releasingSpace.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
        releasingSpace.setKey(KeyboardEvent.KEY_SPACE);
        keyboard.addEventListener(releasingSpace);

    }

    @Override
    public void keyPressed(KeyboardEvent event) {
        switch (event.getKey()) {
            case KeyboardEvent.KEY_UP:
                player.setY(-1);
                break;
            case KeyboardEvent.KEY_DOWN:
                player.setY(1);
                break;
            case KeyboardEvent.KEY_LEFT:
                player.setX(-1);
                break;
            case KeyboardEvent.KEY_RIGHT:
                player.setX(1);
                break;
            case KeyboardEvent.KEY_SPACE:
                player.kick(4);
                sound.playBallKicked(true);

        }
    }

    @Override
    public void keyReleased(KeyboardEvent event) {
        switch (event.getKey()) {
            case KeyboardEvent.KEY_SPACE:
                break;
            case KeyboardEvent.KEY_UP:
            case KeyboardEvent.KEY_DOWN:
                player.setY(0);
                break;
            case KeyboardEvent.KEY_LEFT:
            case KeyboardEvent.KEY_RIGHT:
                player.setX(0);
                break;
        }


    }

}
