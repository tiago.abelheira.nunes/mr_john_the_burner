package org.academiadecodigo.ramsters.group_project;

import org.academiadecodigo.simplegraphicswithoutmargin.graphics.Text;

public class ScoreBoard {

    private static int score = 0;
    private static Text text;

    public ScoreBoard() {
        text = new Text(10,10,"Score: " + score);
    }

    public static void updateScore() {
        text.delete();
        score++;
        text.setText("Score: " + score);
        text.draw();
    }

    public static void showScore() {
    }


}
