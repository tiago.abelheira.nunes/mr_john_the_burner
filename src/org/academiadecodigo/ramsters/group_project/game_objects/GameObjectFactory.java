package org.academiadecodigo.ramsters.group_project.game_objects;

import org.academiadecodigo.ramsters.group_project.field.Field;
import org.academiadecodigo.ramsters.group_project.field.FieldPosition;
import org.academiadecodigo.simplegraphicswithoutmargin.graphics.Color;

public class GameObjectFactory {

    private int counterOfPlayers;
    private int counterOfBalls;
    private int counterOfTargets;
    private Field field;

    public GameObjectFactory(Field field) {
        this.field = field;
        counterOfPlayers = 0;
        this.counterOfBalls = 0;
        this.counterOfTargets = 0;
    }

    public GameObject[] createGameObjects(int numberOfPlayers, int numberOfBalls, int numberOfTargets) {
        GameObject[] gameObjects = new GameObject[numberOfPlayers + numberOfBalls + numberOfTargets];
        for (int i = 0; i < gameObjects.length; i++) {

            if (counterOfPlayers < numberOfPlayers) {
                GameObject player = new Player(field.makePosition(180,180,20,Color.BLUE));
                gameObjects[i] = player;
                counterOfPlayers++;
                continue;
            }
            if (counterOfBalls < numberOfBalls) {
                GameObject ball = new Ball(field.makeBallPosition(200,200,10,Color.WHITE));
                gameObjects[i] = ball;
                counterOfBalls++;
                continue;
            }
            if (counterOfTargets < numberOfTargets) {
                GameObject target = new Target(field.makePosition(30,Color.GREEN));
                gameObjects[i] = target;
                counterOfTargets++;
            }

        }
        return gameObjects;
    }
}
