package org.academiadecodigo.ramsters.group_project.game_objects;
import org.academiadecodigo.ramsters.group_project.field.FieldPosition;

public abstract class GameObject {

    protected FieldPosition position;

    public GameObject(FieldPosition position) {
        this.position = position;
    }

    public abstract void move();

    public abstract void collide(GameObject object);

    public FieldPosition getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return "I'm a game object";
    }
}
