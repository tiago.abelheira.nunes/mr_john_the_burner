package org.academiadecodigo.ramsters.group_project.game_objects;

public enum GameObjectsType {
    PLAYER,
    BALL,
    TARGET
}
