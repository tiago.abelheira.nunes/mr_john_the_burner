package org.academiadecodigo.ramsters.group_project.game_objects;

import org.academiadecodigo.ramsters.group_project.CollisionDetector;
import org.academiadecodigo.ramsters.group_project.field.BallPosition;
import org.academiadecodigo.ramsters.group_project.field.Field;
import org.academiadecodigo.ramsters.group_project.field.FieldPosition;

public class Ball extends GameObject {

    private double slope;
    private double power;
    private boolean safeMove = true;

    public Ball(BallPosition position) {
        super(position);
    }

    @Override
    public BallPosition getPosition() {
        return (BallPosition) super.getPosition();
    }

    @Override
    public void collide(GameObject object) {
        //What happens when the ball collides? Probably best place to call the bounce logic (Except boundaries bounce)
        //Also probably best player to invoke hit method for targets since we can pass out power for damage calculations
    }

    @Override
    public void move() {
         /*
            Formulas to use slope and power(distance) to calculate run(x) and rise(y) movements
            (Line Equation) y=slope*x => slope=y/x => x = y/slope | (Pythagoras) x^2 + y^2 = power^2 => x = (power^2 - y^2) ^ (1/2) => y = (power^2 - x^2) ^ (1/2)
            (Line equation + Pythagoras)
            y = slope*(power^2-y^2)^(1/2)      => Final formula: y = (slope*power)/(slope^2+1)^(1/2)     << y based on slope and power(distance)
            x = (power^2-x^2)^(1/2) / slope    => Final formula: x = power/(slope^2+1)^(1/2)             << X based on slope and power(distance)
        */// Math logic

        // Non-Unitary movement
        double y = (slope * power) / Math.sqrt(Math.pow(slope, 2) + 1);
        double x = power / Math.sqrt(Math.pow(slope, 2) + 1);
        // Convert to int
        int yMov = Math.toIntExact(Math.round(y));
        int xMov = Math.toIntExact(Math.round(x));
        // Chooses Movement Method
        if (safeMove) {
            unitaryMove(xMov, yMov);
            return;
        }
        // Damp movement
        if (power > 0) {
            power--;
        }
        if (power < 0) {
            power++;
        }
        // Execute movement
        position.movePosition(xMov, yMov);
    }

    public double getPower() {
        return power;
    }

    public void unitaryMove(int x, int y) {
        // Saves the final expected position (position the ball will end at)
        int expectedX = x + position.getCenterX();
        int expectedY = y + position.getCenterY();
        //Set unit power
        int p = power > 0 ? 1 : power < 0 ? -1 : 0;
        // Move a step until total steps equals power
        for (int i = 0; i < Math.abs(power); i++) {
            // Get slope for next movement
            double instantSlope = position.slopeTo(expectedX, expectedY);
            // Get next step
            double yStep = instantSlope * p / Math.sqrt(Math.pow(instantSlope, 2) + 1);
            double xStep = p / Math.sqrt(Math.pow(instantSlope, 2) + 1);
            // Convert to int
            int yMov = Math.toIntExact(Math.round(yStep));
            int xMov = Math.toIntExact(Math.round(xStep));
            // Execute step movement
            position.movePosition(xMov, yMov);
        }
        // Damp movement
        if (power > 0) {
            power--;
        }
        if (power < 0) {
            power++;
        }
    }

    public void invertX() {
        double xStep = power / Math.sqrt(Math.pow(slope, 2) + 1);
        double yStep = slope * power / Math.sqrt(Math.pow(slope, 2) + 1);
        slope = yStep / -xStep;
        power *= -1;
    }

    public void invertY() {
        double xStep = power / Math.sqrt(Math.pow(slope, 2) + 1);
        double yStep = slope * power / Math.sqrt(Math.pow(slope, 2) + 1);
        slope = -yStep / xStep;

    }

    public void setMomentum(int power, double slope) {

        this.power = power;
        this.slope = slope;

        /*
        >> OLD CODE <<

        double xFinal = power * Math.cos(Math.toRadians(angle));

        int newAngle = angle;

        for (int i = power - 1; i > 0; i--) {

            int movementAngle = getBestDirection(newAngle);

            int xNext = Math.toIntExact(Math.round(Math.cos(Math.toRadians(movementAngle))));
            int yNext = Math.toIntExact(Math.round(Math.sin(Math.toRadians(movementAngle))));

            position.movePosition(xNext, yNext);

            newAngle = Math.toIntExact(Math.round(Math.toDegrees(Math.acos(xFinal / i))));
        }

         */ // Old code
    }

/*
    private static int getBestDirection(int angle) {

        int[] angleOptions = {0, 45, 90, 135, 180, 225, 270, 315, 360};

        int bestAngle = 0;
        int minDif = 361;

        for (int eachAngle : angleOptions) {

            if (Math.abs(eachAngle - angle) < minDif) {

                bestAngle = eachAngle;
                minDif = Math.abs(eachAngle - angle);
            }
        }

        return bestAngle;
    }

 *///Deprecated getBestDirection
}
