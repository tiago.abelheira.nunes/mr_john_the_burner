package org.academiadecodigo.ramsters.group_project.game_objects;

import org.academiadecodigo.ramsters.group_project.ScoreBoard;
import org.academiadecodigo.ramsters.group_project.field.Field;
import org.academiadecodigo.ramsters.group_project.field.FieldPosition;
import org.academiadecodigo.simplegraphicswithoutmargin.graphics.Color;

import java.util.Random;

public class Target extends GameObject {

    private boolean dead = false;
    private int health;
    private int currentX = 0;
    private int currentY = 0;

    public Target(FieldPosition position) {
        super(position);
        health = 100;

    }

    //needs implementation
    /*@Override
    public void move() {

    }*/

    @Override
    public void collide(GameObject object) {
        if (object instanceof Ball) {
            getPosition().setColor(Color.RED);
            ScoreBoard.updateScore();

            double power = ((Ball) object).getPower() == 0 ? 1 : ((Ball) object).getPower();
            int power2 = Math.toIntExact(Math.round(Math.abs(power)));
            double m = object.getPosition().slopeTo(position);
            if (position.getCenterX() > object.getPosition().getCenterX()) {
                power2 *= -1;
            }
            if (position.getCenterX() < object.getPosition().getCenterX()) {
                power2 *= 1;
            }
            if (position.getCenterX() == object.getPosition().getCenterX()) {
                power2 = position.getCenterY() < object.getPosition().getCenterY() ? -1 : 1;
            }
            // Sets momentum to the ball
            ((Ball) object).setMomentum(power2, m);
            object.move();
        }
    }

    @Override
    public void move() {

        if (new Random().nextInt(100) < 2) {
            currentX = new Random().nextBoolean() ? currentX * -1 : 1;
            currentY = new Random().nextBoolean() ? currentY * -1 : 1;
        }
        super.getPosition().movePosition(currentX, currentY);
    }

    public void hit(int damage) {
        health -= damage;
        if (health < 0) {
            dead = true;
        }
    }

    // this method might be useful to know if the target is dead or not:
    public boolean isDead() {
        return dead;
    }

}
