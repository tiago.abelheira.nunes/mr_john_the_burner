package org.academiadecodigo.ramsters.group_project.game_objects;

import org.academiadecodigo.ramsters.group_project.field.Field;
import org.academiadecodigo.ramsters.group_project.field.FieldPosition;

public class Player extends GameObject {

    private Ball ball;
    int x = 0;
    int y = 0;

    public Player(FieldPosition position) {
        super(position);
    }

    public void setBall(Ball ball) {
        this.ball = ball;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void collide(GameObject object) {

        if(object.equals(ball)) {
            kick(1);
        }


        //What does the player does when colliding? I think it could kick the ball with minimum power!
        //What about colliding with target?
    }

    @Override
    public void move() {
        position.movePosition(x, y);
    }

    public void kick(int power) {
        // Checks distance to the ball to see if it is close enough to kick
        if (position.distanceTo(ball.position) < 5) {
            // Gets the slope
            double m = position.slopeTo(ball.position);
            // Calculates power direction according to quadrant
            if (ball.position.getCenterX() > position.getCenterX()) {
                power *= 1;
            }
            if (ball.position.getCenterX() < position.getCenterX()) {
                power *= -1;
            }
            if (ball.position.getCenterX() == position.getCenterX()) {
                power = ball.position.getCenterY() < position.getCenterY() ? 1 : -1;
            }
            // Sets momentum to the ball
            ball.setMomentum(power * 6, m);
        }
    }
}
