package org.academiadecodigo.ramsters.group_project.game_objects;

import org.academiadecodigo.ramsters.group_project.field.Field;

public class Test {
    public static void main(String[] args) {

        int newAngle = Math.toIntExact(Math.round(Math.toDegrees(Math.acos(2.0d / 5))));
        System.out.println(newAngle);
    }

}
